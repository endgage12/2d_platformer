It is required to create a 2D platformer software application in C ++, which should include the following functions:
 Character management;
 Collision with the enemy;
 Falling into a trap (the field on which the hero dies);
 Display of the game situation;
 Menu at the start and at the end of the game;