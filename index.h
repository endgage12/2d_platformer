class Entity {
public:
	float dx, dy, x, y, speed, moveTimer;
	int w, h;
	bool onGround;
	Texture texture;
	Sprite sprite;
	String name;
	Text texts;
	Entity(Image &image, float X, float Y, int W, int H, String Name, Text text) {
		texts = text;
		x = X; y = Y; w = W; h = H; name = Name;
		speed = 0; dx = 0; dy = 0; onGround = false;
		texture.loadFromImage(image);
		sprite.setTexture(texture);
		sprite.setOrigin(w / 2, h / 2);
		sprite.setPosition(x + w / 2, y + h / 2);	
		texts.setPosition(x - (w /2), y - h);
		if (name == "Key") {
			texts.setString("Press E");
			sprite.setTextureRect(IntRect(0, 0, w, h));
		}
	}
	Entity() {}
};
class Player : public Entity {
public:
	int points = 0;
	enum { left, right, up, down, jump, stay } state;
	bool isLife = true, isKey;

	Player(Image &image, float X, float Y, int W, int H, String Name, Text text) : Entity(image, X, Y, W, H, Name, text) {
		state = stay;
		if (name == "Player1") {
			sprite.setTextureRect(IntRect(0, 0, w, h));
		}
	}

	void control() {
		if (Keyboard::isKeyPressed) {
			if (Keyboard::isKeyPressed(Keyboard::Left)) {
				state = left; speed = 0.5; sprite.setTextureRect(IntRect(100, 0, 50, 50));
			}
			if (Keyboard::isKeyPressed(Keyboard::Right)) {
				state = right; speed = 0.5; sprite.setTextureRect(IntRect(50, 0, 50, 50));
			}
			if ((Keyboard::isKeyPressed(Keyboard::Up)) && (onGround)) {
				state = jump; speed = 0.6; sprite.setTextureRect(IntRect(0, 0, 50, 50));
			}
		}
	}

	void pickUp(Entity key) {
		if (x + w >= key.x && x <= key.x + key.w && y + h >= key.y && y <= key.y + key.h) { //логика столкновения
			if (key.name == "Key" && Keyboard::isKeyPressed(Keyboard::E)) {		
				TileMap[20][75] = ' ';
				TileMap[21][75] = ' ';
				TileMap[22][75] = ' ';
			}
		}
	}

	void checkCollisionWithMap(float Dx, float Dy) {
		for (int i = y / 32; i < (y + h) / 32; i++)
			for (int j = x / 32; j < (x + w) / 32; j++) {
				if (TileMap[i][j] == '0') {
					if (Dy > 0) { y = i * 32 - h;  dy = 0; onGround = true; }//по Y вниз=>идем в пол(стоим на месте) или падаем. В этот момент надо вытолкнуть персонажа и поставить его на землю, при этом говорим что мы на земле тем самым снова можем прыгать
					if (Dy < 0) { y = i * 32 + 32;  dy = 0; }//столкновение с верхними краями карты(может и не пригодиться)
					if (Dx > 0) { x = j * 32 - w; }//с правым краем карты
					if (Dx < 0) { x = j * 32 + 32; }// с левым краем карты
				}
				else if (TileMap[i][j] == 'w') {
					isLife = false;
					if (Dy > 0) { y = i * 32 - h;  dy = 0; onGround = true; }
					if (Dy < 0) { y = i * 32 + 32;  dy = 0; }
					if (Dx > 0) { x = j * 32 - w; }
					if (Dx < 0) { x = j * 32 + 32; }
				}
				else if (TileMap[i][j] == ' ') {}
				else if (TileMap[i][j] == 's') {}
				else if (TileMap[i][j] == '_') {
					endGame();
					if (Dy > 0) { y = i * 32 - h;  dy = 0; onGround = true; }
					if (Dy < 0) { y = i * 32 + 32;  dy = 0; }
					if (Dx > 0) { x = j * 32 - w; }
					if (Dx < 0) { x = j * 32 + 32; }
				}
				else isLife = false;
			}
	}

	void death(Entity e) {
		if (e.x + e.w >= x && e.x <= x + w && e.y + e.h >= y && e.y <= y + h) { //логика столкновения
			if (y > e.y) {
				isLife = false;
			}
			else ++points;
		}
	}

	void gui() {
		texts.setString("Press Tab");
		texts.setPosition(900, 540);
		if (Keyboard::isKeyPressed(Keyboard::Tab)) {
			std::ostringstream pts;
			pts << points;
			texts.setString("Points: " + pts.str());
			texts.setPosition(x, y - 50);
		}
	}

	void endGame() {
		if (points == 3) isLife = false;
	}

	void update(float time) {
		if (isLife) {
			gui();
			control();
			switch (state) {
			case right:dx = speed; break;
			case left:dx = -speed; break;
			case jump:dy = -speed; onGround = false; break;
			}
			speed = 0;

			dy = dy + 0.0015*time; //гравитация
			x += dx * time;
			checkCollisionWithMap(dx, 0);
			y += dy * time;
			checkCollisionWithMap(0, dy);
			sprite.setPosition(x + w / 2, y + h / 2);
			setPlayerCoordinateForView(x, y);
		}
		else {
			texts.setCharacterSize(54);
			texts.setString("Game over");
			texts.setPosition(view.getCenter().x, view.getCenter().y);
			sprite.setTextureRect(IntRect(0, 0, 0, 0));
			x -= 1000;
		}
	}
};
class Enemy : public Entity {
public:
	bool ch = false;
	bool isLife = true;
	Enemy(Image &image, float X, float Y, int W, int H, String Name, Text text) : Entity(image, X, Y, W, H, Name, text) {
		dx = 0.1;
		if (name == "Enemy1") {
			sprite.setTextureRect(IntRect(0, 0, w, h));
			texts.setString("Enemy1");
		}
		else if (name == "Boss") {
			sprite.setTextureRect(IntRect(0, 0, w, h));
			texts.setString("Boss");
		}
	}

	void checkCollisionWithMap(float Dx, float Dy) {
		for (int i = y / 32; i < (y + h) / 32; i++)
			for (int j = x / 32; j < (x + w) / 32; j++) {
				if (TileMap[i][j] == '0') {
					if (Dy > 0) { y = i * 32 - h;  dy = 0; onGround = true; }
					if (Dy < 0) { y = i * 32 + 32;  dy = 0; }
					if (Dx > 0) { x = j * 32 - w; dx = -0.1; sprite.scale(-1, 1); }
					if (Dx < 0) { x = j * 32 + 32; dx = 0.1; sprite.scale(-1, 1); }
				}
				else if (TileMap[i][j] == 'w') {
					isLife = false;
					if (Dy > 0) { y = i * 32 - h;  dy = 0; onGround = true; }
					if (Dy < 0) { y = i * 32 + 32;  dy = 0; }
					if (Dx > 0) { x = j * 32 - w; }
					if (Dx < 0) { x = j * 32 + 32; }
				}
				else if (TileMap[i][j] == ' ') {}
				else if (TileMap[i][j] == 's') {}
				else isLife = false;
			}
	}

	void update(float time, Player p) {
		if (isLife == true) {
			if (p.x + p.w >= x && p.x <= x + w && p.y + p.h >= y && p.y <= y + h) { //логика столкновения
				if (y > p.y) {
					isLife = false;
				}
			}

			dy = dy + 0.0015*time; //гравитация
			x += dx * time;
			checkCollisionWithMap(dx, 0);
			y += dy * time;
			checkCollisionWithMap(0, dy);
			sprite.setPosition(x + w / 2, y + h / 2);
			texts.setPosition(x - (w / 2), y - (h / 2));
		}
		else {
			sprite.setTextureRect(IntRect(0, 0, 0, 0));
			texts.setPosition(-100, -100);
			x -= 1000;
		}
		}
};
void menu(RenderWindow &window) {
	Texture menuTexture, bgTexture, starTexture, star6Texture;
	Sprite menu1, menu2, menu3, bg, star, star6;
	bool isMenu = true;
	int menu_num = 0;

	bgTexture.loadFromFile("C:/Users/Polyashik/YandexDisk/2 курс/2 семестр/Курсовая/Going to the river/img/background.jpg");
	menuTexture.loadFromFile("C:/Users/Polyashik/YandexDisk/2 курс/2 семестр/Курсовая/Going to the river/img/menu.png");
	starTexture.loadFromFile("C:/Users/Polyashik/YandexDisk/2 курс/2 семестр/Курсовая/Going to the river/img/star.jpg");
	star6Texture.loadFromFile("C:/Users/Polyashik/YandexDisk/2 курс/2 семестр/Курсовая/Going to the river/img/star6x6.jpg");

	bg.setTexture(bgTexture);
	bg.setPosition(0, 0);

	star.setTexture(starTexture);
	star.setPosition(50, 50);
	star6.setTexture(star6Texture);
	star6.setPosition(10, 70);

	menu1.setTexture(menuTexture);
	menu1.setTextureRect(IntRect(0, 0, 147, 45));
	menu1.setPosition(406, 210);
	menu2.setTexture(menuTexture);
	menu2.setTextureRect(IntRect(0, 51, 147, 45));
	menu2.setPosition(406, 265);
	menu3.setTexture(menuTexture);
	menu3.setTextureRect(IntRect(0, 102, 147, 45));
	menu3.setPosition(406, 320);

	while (isMenu) {
		int starX = star.getPosition().x, starY = star.getPosition().y;
		int star6X = star6.getPosition().x, star6Y = star6.getPosition().y;
		star.move(0.125, 0);
		star6.move(0.075, 0);
		if (starX > 920) star.setPosition(0, 50);
		if (star6X > 920) star6.setPosition(0, 70);
		
		menu_num = 0;
		if (IntRect(406, 210, 147, 45).contains(Mouse::getPosition(window))) {
			menu1.setTextureRect(IntRect(153, 0, 147, 45));
			menu_num = 1; 
		}
		else if (IntRect(406, 265, 147, 45).contains(Mouse::getPosition(window))) { menu2.setTextureRect(IntRect(153, 51, 147, 45)); menu_num = 2; }
		else if (IntRect(406, 320, 147, 45).contains(Mouse::getPosition(window))) { menu3.setTextureRect(IntRect(153, 102, 147, 45)); menu_num = 3; }
		else {
			menu1.setTextureRect(IntRect(0, 0, 147, 45));
			menu2.setTextureRect(IntRect(0, 51, 147, 45));
			menu3.setTextureRect(IntRect(0, 102, 147, 45));
		}

		if (Mouse::isButtonPressed(Mouse::Left))
		{
			if (menu_num == 1) isMenu = false;
			if (menu_num == 3) { window.close(); isMenu = false; }

		}
		window.draw(bg);
		window.draw(star);
		window.draw(star6);
		window.draw(menu1);
		window.draw(menu2);
		window.draw(menu3);
		window.display();
	}
}
int startGame() {
	RenderWindow window(VideoMode(960, 720), "GTTR");
	menu(window);
	view.reset(FloatRect(0, 0, 960, 720));

	Font font;
	font.loadFromFile("C:/Users/Polyashik/YandexDisk/2 курс/2 семестр/Курсовая/Going to the river/font/16804.otf");
	Text text("", font, 20);

	Image map_image, heroImage, enemy1Image, bossEnemy1Image, keyImage;

	heroImage.loadFromFile("C:/Users/Polyashik/YandexDisk/2 курс/2 семестр/Курсовая/Going to the river/img/hero.jpg");
	enemy1Image.loadFromFile("C:/Users/Polyashik/YandexDisk/2 курс/2 семестр/Курсовая/Going to the river/img/enemy.jpg");
	bossEnemy1Image.loadFromFile("C:/Users/Polyashik/YandexDisk/2 курс/2 семестр/Курсовая/Going to the river/img/bossenemy.jpg");
	keyImage.loadFromFile("C:/Users/Polyashik/YandexDisk/2 курс/2 семестр/Курсовая/Going to the river/img/key.jpg");

	Texture map;
	Sprite s_map;

	map_image.loadFromFile("C:/Users/Polyashik/YandexDisk/2 курс/2 семестр/Курсовая/Going to the river/img/map.jpg");
	map.loadFromImage(map_image);
	s_map.setTexture(map);

	Clock clock;
	Player p(heroImage, 100, 680, 50, 50, "Player1", text);
	Enemy Enemy1(enemy1Image, 1500, 180, 100, 60, "Enemy1", text);
	Enemy Boss1(bossEnemy1Image, 900, 680, 50, 51, "Boss", text);
	Enemy Boss2(bossEnemy1Image, 900, 330, 50, 51, "Boss", text);
	Entity key(keyImage, 2040, 630, 30, 30, "Key", text);

	while (window.isOpen()) {
		Event event; //Event: ловит состояние закрывания окна
		float time = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		time = time / 1400;

		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				window.close();
		}

		p.update(time);
		Enemy1.update(time, p);
		Boss1.update(time, p);
		Boss2.update(time, p);
		window.setView(view);
		window.clear();

		for (int i = 0; i < HEIGHT_MAP; i++)
			for (int j = 0; j < WIDTH_MAP; j++) {
				if (TileMap[i][j] == ' ')  s_map.setTextureRect(IntRect(0, 0, 32, 32));
				if (TileMap[i][j] == 's')  s_map.setTextureRect(IntRect(32, 0, 32, 32));
				if (TileMap[i][j] == '0') s_map.setTextureRect(IntRect(64, 0, 32, 32));
				if (TileMap[i][j] == 'w') s_map.setTextureRect(IntRect(96, 0, 32, 32));
				s_map.setPosition(j * 32, i * 32);
				window.draw(s_map);
			}

		//LOGIC
		p.death(Enemy1);
		p.death(Boss1);
		p.death(Boss2);
		p.pickUp(key);
		if (p.isLife == false && Keyboard::isKeyPressed(Keyboard::E)) return 1;

		for (int i = 0; i < HEIGHT_MAP; i++) { //логика открывания дверей
			for (int j = 0; j < WIDTH_MAP; j++) {
				if (Enemy1.isLife == false) {
					if (TileMap[i][j] == 's') TileMap[i][j] = '0';
				}
			}
		}

		window.draw(key.texts);
		window.draw(key.sprite);
		window.draw(Boss1.texts);
		window.draw(Boss1.sprite);
		window.draw(Boss2.texts);
		window.draw(Boss2.sprite);
		window.draw(Enemy1.texts);
		window.draw(Enemy1.sprite);
		window.draw(p.texts);
		window.draw(p.sprite);
		window.display();
	}
	
	return 0;
}
int main() {
	while(startGame() == 1) startGame();
	return 0;
}
